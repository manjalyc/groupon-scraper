import time
from selenium import webdriver
#Use Chrome options, if chrome is installed allows visualization and easy debugging
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException     

#Cache Imports
import ast
from os.path import exists as os_path_exists
from os import listdir
from os.path import isfile, join

global driver, interval, inputUrlFile, outputCSV
outputCSV = './output.csv'
inputUrlFile = './target_urls'
interval = 1

cacheDataDir = './CACHE'

def getCacheID(url):
	if url.index('https://www.groupon.com/deals/') == 0: return url[30:]
	raise ValueError(f'BAD URL {url} for getCacheID')

def inCache(cacheID):
	global cacheDataDir
	return os_path_exists(cacheDataDir + '/' + cacheID)

def getFromCache(cacheID):
	global cacheDataDir
	print(f'[CACHE][PULL]: {cacheID}')
	ret = ''
	with open(cacheDataDir + '/' + cacheID,'r') as iw:
		r = iw.read()
		ret = ast.literal_eval(r) 
	return ret

def putIntoCache(cacheID, data):
	global cacheDataDir
	print(f'[CACHE][INST]: {cacheID}')
	with open(cacheDataDir + '/' + cacheID,'w') as ow:
		ow.write(str(data))
	return


def startWebDriver():
	global driver
	options = Options()
	driver = webdriver.Chrome(chrome_options=options)

def closeWebDriver():
	global driver
	driver.close()

def retrieveURLList(urlFile):
	urls = []
	for line in open(urlFile, 'r'): urls.append(line.strip())
	return urls

def xpathExists(xpath):
	global driver
	try:
		driver.find_element_by_xpath(xpath)
	except NoSuchElementException:
		return False
	return True

def getElement(xpath):
	global driver
	return driver.find_element_by_xpath(xpath)

def moveToAndClick(xpath):
	global driver
	element = getElement(xpath)
	ActionChains(driver).move_to_element(element).click(element).perform()
	return

def clickIfExists(xpath):
	if xpathExists(xpath):
		moveToAndClick(xpath)
		return True
	return False

'''
def clickIfExistsClassNameWithOffset(className, ox, oy):
	global driver
	try:
		element = driver.find_element_by_class_name(className)
		ActionChains(driver).move_to_element_with_offset(element,ox,oy).click().perform()
	except NoSuchElementException:
		return False
	return True	
'''

def getText(xpath):
	element = getElement(xpath)
	return element.text

def cutX(x, r):
	return x[ x.index(r) + len(r):]

def removeFirstRinX(r, x):
	if r in x: return x[ x.index(r) + len(r):]
	return x

def getTagsOnPage(htmlData):
	if '<div class="aspects aspect-list-collapsed">' not in htmlData: return []
	x = cutX(htmlData, '<div class="aspects aspect-list-collapsed">')
	x = cutX(x, '<div class="aspect">')
	if '<div class="see-more-aspect-link">' not in x:
		x = x[:x.index('</div></div>')]
	else: x = x[:x.index('<div class="see-more-aspect-link">')]
	x = x.replace('</div>','').strip()
	return x.split('<div class="aspect">')

def getRatingsOnPage():
	global driver
	xaR1='/html/body/section[2]/div/section[2]/div/div[1]/div[1]/div[3]/a/span[1]'
	xaR2='/html/body/section[2]/div/section[2]/div/div[1]/div[1]/div[2]/a/span[1]'
	xnR1='/html/body/section[2]/div/section[2]/div/div[1]/div[1]/div[3]/a/span[2]/span'
	xnR2='/html/body/section[2]/div/section[2]/div/div[1]/div[1]/div[2]/a/span[2]/span'
	avgRating = ''
	numRatings = ''
	if xpathExists(xaR1):
		avgRating=getText(xaR1)
		numRatings=int(getText(xnR1).split()[0].replace(',',''))
	elif xpathExists(xaR2):
		avgRating=getText(xaR2)
		numRatings=int(getText(xnR2).split()[0].replace(',',''))
	else:
		avgRating='NA'
		numRatings='NA'
	return (avgRating, numRatings)

def getProductsAndPricesOnPage():
	products = []
	currPrices = []
	basePrices = []
	buys = []
	xsingleOption='/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form[1]/fieldset/ul/li/div/label/div/h3'
	xmultiOptionBase='/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form/fieldset/ul/li['
	xmultiOptionEnd=']/div/label/div/h3'
	xmultiOptionNum=1
	xmultiOption=xmultiOptionBase+str(xmultiOptionNum)+xmultiOptionEnd
	
	def setProductInfo(xcP, xbP, xB):
		currPrices.append(getText(xcP))
		if xpathExists(xbP): basePrices.append(getText(xbP))
		else: basePrices.append(getText(xcP))
		if xpathExists(xB): buys.append(getText(xB))
		else: buys.append('NA')

	#Single Option
	'''
	if xpathExists(xsingleOption):
		xcP = '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form[1]/fieldset/ul/li/div/label/div/div/div[2]/div[1]/div[2]/div'
		xbP = '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form[1]/fieldset/ul/li/div/label/div/div/div[2]/div[1]/div[1]/div'
		xB =  '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form[1]/fieldset/ul/li/div/label/div/div/div[1]/div'
		products.append(getText(xsingleOption))
		setProductInfo(xcP, xbP, xB)
	elif not xpathExists(xmultiOption): print('\n-----------------' * 102)
	'''
	
	#Multi Option
	while xpathExists(xmultiOption):
		xcP = '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form/fieldset/ul/li[' + str(xmultiOptionNum) + ']/div/label/div/div/div[2]/div[1]/div[2]/div'
		xbP = '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form/fieldset/ul/li[' + str(xmultiOptionNum) + ']/div/label/div/div/div[2]/div[1]/div[1]/div'
		xB =  '/html/body/section[2]/div/section[2]/div/div[2]/div[1]/div/div[4]/form/fieldset/ul/li[' + str(xmultiOptionNum) + ']/div/label/div/div/div[1]/div'
		products.append(getText(xmultiOption))
		setProductInfo(xcP, xbP, xB)
		xmultiOptionNum += 1
		xmultiOption=xmultiOptionBase+str(xmultiOptionNum)+xmultiOptionEnd
	return [products, currPrices, basePrices, buys]

global cleaner, newlines
newlines = ['\u2028','\r','\v','\x0b','\f','\x0c','\x1c','\x1d','\x1e','\x85','\u2028','\u2029','*',' ;']
import re
cleaner = re.compile('<.*?>')
def sanitize(x):
	for i in newlines: x = x.replace(i,';')
	return re.sub(cleaner, '', x).replace('\xa0',' ').strip().replace(' \n ','; ').replace('\n',';').replace('  ',' ').replace('\t','  ').replace(';;',';')

def getSectionOnPage(xpath):
	if xpathExists(xpath):
		return sanitize(getText(xpath))
	return "NA"


def findExistingXPathI(sr, en):
	for i in range(1,10):
		if xpathExists(sr + str(i) + en): return i
	return -1

def getReviewsOnPage():
	global interval
	if not xpathExists('//*[@id="tips"]'): return []
	
	#determine seeMoreReviewsButton
	xSeeAllReviewsB='/html/body/section[2]/div/section[2]/div/div[2]/div[2]/section/div/div/article/div['
	xSeeAllReviewsE=']/div[4]/a'
	xSeeAllReviews= ''
	cde = False
	for i in range(1,11):
		xSeeAllReviews = xSeeAllReviewsB + str(i) + xSeeAllReviewsE
		if xpathExists(xSeeAllReviews):
			cde = True
			#print("SD",i)
			break
	
	
	#click SeeMoreReviews
	if not cde:
		reviews = []
		xrb1 = '/html/body/section[2]/div/section[2]/div/div[2]/div[2]/section/div/div/article/div['
		#'/html/body/section[2]/div/section[2]/div/div[2]/div[2]/section/div/div/article/div[6]/div[4]/div/div'
		#'/html/body/section[2]/div/section[2]/div/div[2]/div[2]/section/div/div/article/div[2]/div[4]/div/div'
		xre1 = ']/div[4]/div[3]/div'
		xri = findExistingXPathI(xrb1, xre1)
		if xri > 0:
			xr = xrb1 + str(xri) + xre1
			reviews.append(sanitize(getText(xr).split('\n')[-2]))
		else:
			xrb2 = xrb1
			xre2 = ']/div[4]/div/div'
			xri = findExistingXPathI(xrb2, xre2)
			xr = xrb2 + str(xri) + xre2
			if xri > 0: reviews.append(sanitize(getText(xr).split('\n')[-2]))
			#print(sanitize(getText(xr).split('\n')[-2]))
		#essential-health-and-healing-hands
		
		if len(reviews) > 0: return reviews #comment out to force error to get all reviews when there are no extra
		if "This deal doesn't have any written reviews yet." in driver.page_source:
			return reviews
		print("ASDF")
		print(reviews)
	
	ActionChains(driver).move_to_element(getElement(xSeeAllReviews)).click().perform()
	time.sleep(interval)
		
		
		
		

	#Determine xNextButton
	xNextButtonB='/html/body/div['
	xNextButtonE=']/div[2]/div[5]/div[2]'
	for i in range(1,11):
		xNextButton = xNextButtonB + str(i) + xNextButtonE
		if xpathExists(xNextButton):
			#print("ND",i)
			break
	
	
	reviews = []
	xrE = ']/div[3]'
	xrB = ''
	cont = True
	#print("Entering Loop")
	while cont:
		time.sleep(interval)
		c = 1
		#Determine xrB
		preXRB = '/html/body/div['
		postXRB = ']/div[2]/div[4]/div[1]'
		for i in range(1,10):
			if xpathExists(preXRB+str(i)+postXRB):
				xrB=(preXRB+str(i)+postXRB)[:-2]
				break
		
		xr = xrB + str(c) + xrE
		#print(xr)
		#if xpathExists(xr+''): print(getText(xr+'/div[3]'))
		while(xpathExists(xr)):
			#print(f'exists {xr}')
			#currReview = sanitize(getText(xr).split('\n')[-2])
			currReview = sanitize(getText(xr))
			#print('\n', currReview,'\n')
			if currReview in reviews:
				cont = False
				break
			reviews.append(currReview)
			lastReview = currReview
			c += 1
			#print(c)
			xr = xrB + str(c) + xrE
		#print("Clicking Next")
		clickIfExists(xNextButton)
	#print(len(reviews))
	return reviews



def getData(url):
	global driver
	#open url
	print(f'[PROCESSING]: {url}')
	driver.get(url)
	
	#ensure page load
	time.sleep(interval)
	
	#verify no change on page url
	if driver.current_url != url:
		print("BAD URL: " + url)
		return False
	
	#No Thanks to sign up
	clickIfExists('//*[@id="nothx"]')
	
	title = getText('//*[@id="deal-title"]')
	xLoc = '/html/body/section[2]/div/section[2]/div/div[1]/div[2]/div/h2/span/div/a'
	while not xpathExists(xLoc):
		time.sleep(interval)
	location = getText(xLoc)
	
	#Get Avg & # of Ratings
	avgRating, numRatings = getRatingsOnPage()
	#Get Tags
	tags = getTagsOnPage(driver.page_source)
	
	#Get Products, Prices
	products, currPrices, basePrices, buys = getProductsAndPricesOnPage()
	#Get Sections (if Available)
	highlights = getSectionOnPage('//*[@id="highlights"]')
	
	aboutDeal = getSectionOnPage('//*[@id="about-deal"]')
	
	finePrint = getSectionOnPage('//*[@id="fine-print"]')
	aboutMerchant = getSectionOnPage('//*[@id="about-merchant"]')

	
	#reviews
	reviews = getReviewsOnPage()

	#Print Information
	def productsPrint(products, basePrices, currPrices, buys): 
		for i in range(len(products)): print('\t\t', products[i], basePrices[i], currPrices[i], buys[i])
	
	print('\t',title, avgRating, numRatings, location)
	productsPrint(products, basePrices, currPrices, buys)
	print('\t',tags)
	print('\t',len(reviews))
	#print('\t', reviews)
	#print('\t\t\t', highlights)
	#print('\t\t\t', aboutDeal)
	#print('\t\t\t', finePrint)
	#print('\t\t\t', aboutMerchant)
	
	ret = dict()
	ret['title']=title
	ret['avgRating']=avgRating
	ret['numRatings']=numRatings
	ret['location']=location
	ret['products']=products
	ret['basePrices']=basePrices
	ret['currPrices']=currPrices
	ret['buys']=buys
	ret['tags']=tags
	ret['highlights']=highlights
	ret['aboutDeal']=aboutDeal
	ret['finePrint']=finePrint
	ret['aboutMerchant']=aboutMerchant
	ret['reviews']=reviews
	return ret

def strListToCommaSeperated(x):
	ret = ''
	for i in x: ret += i + ", "
	return ret[:-2]
	

def writeCSV(data, outputCSV):
	o = open(outputCSV,'w')
	
	#QuickFix: TODO REWORK DATABASE
	'''
	for url in data:
		if data[url] == False: continue 
		if len(data[url]['products']) > 0 and data[url]['products'][0] == data[url]['products'][1]:
			del data[url]['products'][0]
			del data[url]['basePrices'][0]
			del data[url]['buys'][0]
			del data[url]['currPrices'][0]
	'''

	
	o.write('Title\tAvg. Rating\tTotal Ratings\tLocation\tTags\t')
	#number of products
	pros = 0
	for url in data:
		if data[url] == False: continue
		pros = max(len(data[url]['products']),pros)
	
	o.write('URL\tHighlights\tAbout Deal\tFine Print\tAbout Merchant\t')
	for i in range(1,pros+1):
		o.write(f"Product #{i}\tCurrent Price #{i}\tBase Price #{i}\tAmount Bought #{i}\t")	
	o.write('Reviews...\n')
	
	for url in data:
		if data[url] == False: continue
		highlights = data[url]['highlights']
		aboutDeal = data[url]['aboutDeal']
		finePrint = data[url]['finePrint']
		aboutMerchant = data[url]['aboutMerchant']
		highlights = removeFirstRinX('Why We Like It;', highlights)
		highlights = removeFirstRinX('Highlights;', highlights)
		aboutDeal = removeFirstRinX('About This Deal;', aboutDeal)
		aboutDeal = removeFirstRinX("What You'll Get;", aboutDeal)
		finePrint = removeFirstRinX("Need to Know;Fine Print and Helpful Information;", finePrint)
		finePrint = removeFirstRinX("The Fine Print;", finePrint)
		aboutMerchant = removeFirstRinX("About ", aboutMerchant)
		data[url]['highlights'] = highlights
		data[url]['aboutDeal'] = aboutDeal
		data[url]['finePrint'] = finePrint
		data[url]['aboutMerchant']=aboutMerchant
	
	for url in data:
		if data[url] == False:
			o.write(f'Bad URL: {url}'+ '\n')
			continue
		o.write(data[url]['title'] + '\t')
		o.write(data[url]['avgRating'] + '\t')
		o.write(str(data[url]['numRatings']) + '\t')
		o.write(data[url]['location'] + '\t')
		o.write(strListToCommaSeperated(data[url]['tags']) + '\t')
		o.write(url + '\t')		
		o.write(data[url]['highlights'] + '\t')
		o.write(data[url]['aboutDeal'] + '\t')
		o.write(data[url]['finePrint'] + '\t')
		o.write(data[url]['aboutMerchant'] + '\t')
		npros = len(data[url]['products'])
		for i in range(npros):
			o.write(data[url]['products'][i] + '\t')
			o.write(data[url]['currPrices'][i] + '\t')
			o.write(data[url]['basePrices'][i] + '\t')
			o.write(data[url]['buys'][i] + '\t')
		for i in range(pros-npros):
			o.write('\t\t\t\t')
		for review in data[url]['reviews']: o.write(review + '\t')
		o.write('\n')
	
	
	o.close()

if __name__ == "__main__":
	global driver
	urls = retrieveURLList(inputUrlFile)
	startWebDriver()
	data = dict()
	#putIntoCache('essential-health-and-healing-hands-1-1', getData('https://www.groupon.com/deals/essential-health-and-healing-hands-1-1'))
	#putIntoCache('dexafit-kansas-city', getData('https://www.groupon.com/deals/dexafit-kansas-city'))
	#https://www.groupon.com/deals/dexafit-kansas-city
	#getData('a')
	#time.sleep(10)
	#getData('https://www.groupon.com/deals/vegas-baby-4d-ultrasound-1')
	#print(getFromCache('dexafit-las-vegas-9'))
	#time.sleep(3)
	for url in urls:
		cID = getCacheID(url)
		data[url] = ''
		if inCache(cID): data[url] = getFromCache(cID)
		else:
			data[url] = getData(url)
			putIntoCache(cID, data[url])
	closeWebDriver()
	writeCSV(data, outputCSV)
